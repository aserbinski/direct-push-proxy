<?php
$msgq_en = false;
$redis_en = true;

$redis_host = "127.0.0.1";
$redis_port = 6379;
$redis_user = null;
$redis_pass = null;
$redis_db = 1;

function err_die($message){
        error_log("Nextcloud/Push Proxy (receiver): ".$message);
        exit(1);
}

if (!class_exists('Redis')) $redis_en = false;
if ((!$redis_en && !$msgq_en) || ($redis_en && $msgq_en))
	err_die("Exactly one message transport must be selected.");

$data = stripcslashes(urldecode(file_get_contents('php://input')));
$data = explode("&", $data);

$devblock = json_decode(substr($data[0], strpos($data[0], "=")+1), true);
if (!isset($devblock['deviceIdentifier']) || !isset($devblock['pushTokenHash']))
        err_die("Message must include deviceIdentifier and pushTokenHash.");

if ($redis_en){
	$redis = new Redis();
	$redis->connect($redis_host, $redis_port) or err_die ("Redis connect error.");
	if ($redis_user != null && $redis_pass != null)
		$redis->auth([$redis_user, $redis_pass]) or err_die ("Redis authentication error.");
	$redis->select($redis_db) or err_die ("Redis database select error.");
}

if ($msgq_en){
	function djb_hash($str){
	        for ($i = 0, $h = 5381, $len = strlen($str); $i < $len; $i++) {
	                $h = (($h << 5) + $h + ord($str[$i])) & 0x7FFFFFFF;
	        }
	        return $h;
	}

	$key = djb_hash($devblock['pushTokenHash']);
	msg_queue_exists($key) or err_die ("Message queue does not exist.");
	$queue = msg_get_queue($key, 0600);
}

foreach($data as $message){
	if ($msgq_en) msg_send($queue, 1, substr($message, strpos($message, "=")+1));
	if ($redis_en) $redis->lPush($devblock['pushTokenHash'].'_'.$devblock['deviceIdentifier'], substr($message, strpos($message, "=")+1));
}

echo "success\n";
?>
