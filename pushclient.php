<?php
ini_set("default_socket_timeout", 6000);

$secure = true;
$dbname = "nextcloud";
$dbtableprefix = "nc_";
$dbhost = "localhost";
$dbuser = "nextcloud";
$dbpassword = "myncpass";

$msgq_en = false;
$redis_en = true;

$redis_host = "127.0.0.1";
$redis_port = 6379;
$redis_user = null;
$redis_pass = null;
$redis_db = 1;

if (ob_get_level()) ob_end_clean();
ob_implicit_flush();

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

function err_die($message){
	error_log("Nextcloud/Push Proxy (sender): ".$message);
	exit(1);
}

if (!class_exists('Redis')) $redis_en = false;
if ((!$redis_en && !$msgq_en) || ($redis_en && $msgq_en))
	err_die("Exactly one message transport must be selected.");

if (!isset($_POST['deviceIdentifiers']) || !isset($_POST['pushTokenHash']))
        err_die("You must POST deviceIdentifiers and pushTokenHash.");

if ($secure){
	$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbname);
	if ($conn->connect_error){
		echo "event: error\n";
		echo "data: database connect failure\n\n";
		flush();
		err_die("Connection failure: " . $conn->connect_error);
	}

	$sql = "SELECT token FROM " . $dbtableprefix . "notifications_pushtokens WHERE pushtokenhash='" . $_POST['pushTokenHash'] ."'";
	$result = $conn->query($sql);

	if ($result->num_rows <= 0){
		echo "event: error\n";
		echo "data: unauthorized\n\n";
		flush();
		err_die("UNAUTHORIZED");
	}
}

$redis = false;
$requeue = false;
$rkey = "";
$rdata = "";
$msgq = false;
register_shutdown_function(function(){
	global $redis_en;
	global $msgq_en;
	global $msgq;
	global $redis;
	global $requeue;
	global $rkey;
	global $rdata;
	if ($requeue){
		if ($redis_en) $redis->rPush($rkey, $rdata);
		if ($msgq_en) msg_send($msgq, 1, $rdata);
	}
});

echo "event: start\n";
echo "data: start\n\n";
flush();

if ($msgq_en){
	function djb_hash($str){
	        for ($i = 0, $h = 5381, $len = strlen($str); $i < $len; $i++) {
	                $h = (($h << 5) + $h + ord($str[$i])) & 0x7FFFFFFF;
	        }
	        return $h;
	}

	$key = djb_hash($_POST['pushTokenHash']);
	if (msg_queue_exists($key)){
		$msgq = msg_get_queue($key, 0600);
		while (msg_receive($msgq, 0, $msgtype, 4096, $message, true, MSG_IPC_NOWAIT)) if ($msgtype == 1){
	                echo "event: notification\n";
	                echo "data: $message\n\n";
	                flush();
	        }
		msg_remove_queue(msg_get_queue($key, 0600));
	}
	$msgq = msg_get_queue($key, 0600);

	// send shutdown message to any running earlier process reading from this queue.
	msg_send($msgq, 1, "shutdown_".getmypid());

	$pingcounter = 0;
	while (true){
		if (msg_receive($msgq, 0, $msgtype, 4096, $message, true, MSG_IPC_NOWAIT) && $msgtype == 1){
			if (strpos($message, 'shutdown') === 0 && getmypid() != explode("_", $message)[1]) return(0);
			$requeue = true;
			$rdata = $message;
			echo "event: notification\n";
			echo "data: $message\n\n";
			flush();
			$requeue = false;
		}
		$pingcounter++;
		if ($pingcounter >= 300){ // about 5 minutes at 1 second per increment.
			echo "event: ping\n";
	                echo "data: timer\n\n";
			flush();
			$pingcounter = 0;
		}
		sleep(1);
	}
}

if ($redis_en){
        $redis = new Redis();
        $redis->connect($redis_host, $redis_port) or err_die ("Redis connect error.");
        if ($redis_user != null && $redis_pass != null)
                $redis->auth([$redis_user, $redis_pass]) or err_die ("Redis authentication error.");
	$redis->select($redis_db) or err_die ("Redis database select error.");

	$keys = array();
	$deviceIdentifiers = explode(",", $_POST['deviceIdentifiers']);
	foreach($deviceIdentifiers as $value){
		$key = $_POST['pushTokenHash'].'_'.$value;
		$keys[] = $key;
		$redis->rPush($key, "shutdown_".getmypid());
	}

	while (true){
		$element = $redis->brPop($keys, 300);
		if ($element !== false && is_array($element) && array_key_exists(1, $element)){
			if (strpos($element[1], 'shutdown') === 0){
				if (getmypid() != explode("_", $element[1])[1]) return(0);
				else continue;
			}
			$requeue = true;
			$rkey = $element[0];
			$rdata = $element[1];
			echo "event: notification\n";
			echo "data: ".$element[1]."\n\n";
			flush();
			$requeue = false;
		} else {
			echo "event: ping\n";
			echo "data: timer\n\n";
			flush();
		}
	}
}

?>
